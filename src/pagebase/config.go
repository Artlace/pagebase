package main

import (
	"log"
	"fmt"
	"io/ioutil"	
	"strings"
)

var Config map[string]string


func getConfigDefaults() {
	Config["permalink_format"] = "/%year&/%month%/%postname%"
}


func pullConfig(d *Database) {
	var param, arg string	

	rows, err := d.db.Query("select parameter, argument from config")

	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		rows.Scan(&param, &arg)
		Config[param] = arg
	}
}

func loadConfig() error {	
	Config = make(map[string]string)

// --- Pull up initial config from file

	ConfContent, err := ioutil.ReadFile("pagebase.config")
	if err != nil {
		fmt.Printf("Can't open config file!\n")
		return err
	}	
	ConfLines := strings.Split(string(ConfContent), "\n")
	
	for _, ConfLine := range ConfLines {
		ConfTuple := strings.Fields(string(ConfLine))
		
		if len(ConfTuple) >= 2 {		
			if strings.IndexAny("#", ConfTuple[0]) == 0 {
				fmt.Printf("Config comment: %s\n", ConfLine)
			} else {
					Config[ConfTuple[0]] = ConfTuple[1]
					fmt.Printf("Parsed config: %s %s\n", ConfTuple[0], ConfTuple[1])
			}
		}	
	}

// --- Initialize defaults
	getConfigDefaults()

// --- Pull rest of the config from database
// ..... will happen from main.

	return nil
}


