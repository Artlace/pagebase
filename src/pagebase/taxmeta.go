/*
	This is where taxonomies and other meta data are kept.
	Basically it's an (at most) 3 level deep tree structure implemented in SQL.

	The naming convention of the node names are:

	PB_<TYPE>

	Examples:

	PB_MENU

	PB_CATEGORY
	PB_TAG

	Each entry has a type, either: T_ROOT or T_LEAF.
	ROOT entries may be orphaned or not. The only way to tell is to query for nodes with its id as parent.

	values could be initialized with iota-s but it's better to make our own mess.

	An entry can have a value up to 256 chars long.
*/

/* ------------------------------------------------------------------

PB_CAT 		- a T_ROOT entry, contains a category name.
PB_TAG 		- a T_ROOT entry, contains a tag name

PB_MENU 	- a T_ROOT entry, contains a menu name
PB_MENU_SUB - a T_SUB entry, contains a submenu entry pointing to its parent which also can be a submenu

PB_TAG_REL - a T_LEAF entry, contains a tag id and a post entry (from posts table) for parent.
PB_CAT_REL - a T_LEAF entry, contains a category id and a post entry (from posts table) for parent.




PB_CAT_REL ----> value:3 parent: 7 ---> the category with id 3 (in same table) points to the post id 7 (in posts table)

--------------------------------------------------------------------- */

package main
